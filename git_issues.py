import os, requests, subprocess
from dotenv import dotenv_values

env = {
  **dotenv_values(".env.local"),
  **os.environ
}

USERNAME = env.get('VUE_APP_DEVELOPER_USERNAME', '')
PROJECT = ""  #TODO: search in gitlab api

url_issue = f'https://gitlab.com/api/v4/issues?\
assignee_username={USERNAME}\
&state=opened\
&milestone=frontend'

headers = {
  'PRIVATE-TOKEN': env.get('VUE_APP_DEVELOPER_TOKEN')
}

if __name__ == "__main__":
  response = requests.get(url_issue, headers=headers)
  issues = response.json()
  
  for (index, issue) in enumerate(issues):
    print(f"{index + 1} >>> {issue.get('labels')[0]}: {issue.get('title')}\n{issue.get('description')}")

  print("Выбери номер задачи:")
  number = int(input())
  issue = issues[number - 1]

  print("Введи название новой ветки.\nСлова разделять знаком тире!")
  branch = input()

  subprocess.run(['git', 'checkout', '-b', branch])
  subprocess.run(['git', 'add', '.'])
  subprocess.run(['git', 'commit', '-m', "'init new branch'"])
  subprocess.run(['git', 'push', 'origin', branch])

  url_mr = f"https://gitlab.com/api/v4/projects/{issue.get('project_id')}/merge_requests"
  data = {
    'title': issue.get('title'),
    'description': issue.get('description'),
    'source_branch': branch,
    'target_branch': 'master',
    'remove_source_branch': True
  }

  response = requests.post(url_mr, headers=headers, data=data)
  print('Done!')
