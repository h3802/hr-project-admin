import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import store from "@/store";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: "/dashboard",
    component: () => import("@/layout/Layout.vue"),
    children: [
      {
        path: "/dashboard",
        name: "dashboard",
        component: () => import("@/views/Dashboard.vue"),
      },
      {
        path: "/account",
        name: "account",
        component: () => import("@/views/account/Account.vue"),
        children: [
          {
            path: "overview",
            name: "account-overview",
            component: () => import("@/views/account/Overview.vue"),
          },
          {
            path: "settings",
            name: "account-settings",
            component: () => import("@/views/account/Settings.vue"),
          },
          {
            path: "worksheets",
            name: "my-worksheets",
            component: () => import("@/views/worksheets/WorksheetListing.vue"),
          },
        ]
      },
      {
        path: "/positions/position-listing",
        name: "position-listing",
        component: () => import("@/views/positions/PositionListing.vue"),
      },
      {
        path: "/positions/position-details/:id",
        name: "position-details",
        component: () => import("@/views/positions/PositionDetails.vue"),
      },
      {
        path: "/members/member-listing",
        name: "member-listing",
        component: () =>
          import("@/views/members/MemberListing.vue"),
      },
      {
        path: "/members/member-details/:id",
        name: "member-details",
        component: () =>
          import("@/views/members/MemberDetails.vue"),
      },
      {
        path: "worksheets/worksheet-details/:id",
        name: "worksheet-details",
        component: () => import("@/views/worksheets/WorksheetDetails.vue"),
      },
    ]
  },
  {
    path: "/",
    component: () => import("@/layout/user/Auth.vue"),
    children: [
      {
        path: "/sign-in",
        name: "sign-in",
        component: () => import("@/views/authentication/SignIn.vue"),
      },
      {
        path: "/sign-up",
        name: "sign-up",
        component: () => import("@/views/authentication/SignUp.vue"),
      },
      {
        path: "/password-reset",
        name: "password-reset",
        component: () => import("@/views/authentication/PasswordReset.vue"),
      },
      {
        path: "/activation/:uid/:token",
        name: "activation",
        component: () => import("@/views/authentication/Activation.vue"),
      }
    ],
  },
  {
    path: "/candidate/:id",
    component: () => import("@/layout/user/Candidate.vue"),
    children: [
      {
        path: "sign-in",
        name: "candidate-sign-in",
        component: () => import("@/views/authentication/candidate/SignIn.vue"),
      },
      {
        path: "sign-up",
        name: "candidate-sign-up",
        component: () => import("@/views/authentication/candidate/SignUp.vue"),
      },
      {
        path: "password-reset",
        name: "candidate-password-reset",
        component: () => import("@/views/authentication/candidate/PasswordReset.vue"),
      },
      {
        path: "worksheet-complete",
        name: "worksheet-complete",
        component: () => import("@/views/worksheets/WorksheetComplete.vue")
      },
    ],
  },
  {
    path: "/404",
    name: "404",
    component: () => import("@/views/Error404.vue"),
  },
  {
    path: "/500",
    name: "500",
    component: () => import("@/views/Error500.vue"),
  },
  {
    path: "/:pathMatch(.*)*",
    redirect: "/404",
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach(() => {
  store.dispatch('VERIFY_AUTH');

  setTimeout(() => {
    window.scrollTo(0, 0);
  }, 100);
});

export default router;
