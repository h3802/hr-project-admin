enum Actions {
  ADD_BODY_CLASSNAME = "addBodyClassName",
  REMOVE_BODY_CLASSNAME = "removeBodyClassName",
  ADD_BODY_ATTRIBUTE = "addBodyAttribute",
  SET_BREADCRUMB_ACTION = "setBreadcrumbAction",
}

enum Mutations {
  SET_CLASSNAME_BY_POSITION = "appendBreadcrumb",
  SET_BREADCRUMB_MUTATION = "setBreadcrumbMutation",
}

export { Actions, Mutations };
