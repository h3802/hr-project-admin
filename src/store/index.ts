import { createStore } from "vuex";
import { config } from "vuex-module-decorators";

import ApiErrorsModule from "@/store/modules/ApiErrorsModule"
import AuthModule from "@/store/modules/AuthModule";
import BodyModule from "@/store/modules/BodyModule";
import BreadcrumbsModule from "@/store/modules/BreadcrumbsModule";
import ConfigureFilesModule from "@/store/modules/ConfigureFilesModule";
import PositionModule from '@/store/modules/PositionModule';
import WorksheetModule from '@/store/modules/WorksheetModule';

config.rawError = true;

const store = createStore({
  modules: {
    ApiErrorsModule,
    AuthModule,
    BodyModule,
    BreadcrumbsModule,
    ConfigureFilesModule,
    PositionModule,
    WorksheetModule,
  },
});

export default store;
