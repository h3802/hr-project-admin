import { Module, VuexModule, Action } from "vuex-module-decorators";
import { Actions } from "@/store/enums/StoreEnums";

@Module
export default class BodyModule extends VuexModule {
  @Action
  [Actions.ADD_BODY_CLASSNAME](className) {
    document.body.classList.add(className);
  }

  @Action
  [Actions.REMOVE_BODY_CLASSNAME](className) {
    document.body.classList.remove(className);
  }

  @Action
  [Actions.ADD_BODY_ATTRIBUTE](payload) {
    const { qulifiedName, value } = payload;
    document.body.setAttribute(qulifiedName, value);
  }
}
