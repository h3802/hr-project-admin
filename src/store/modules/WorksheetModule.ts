import ApiService from "@/core/services/ApiService";
import { AxiosRequestConfig } from "axios";
import { Module, Action, Mutation, VuexModule } from "vuex-module-decorators";
import { User } from '@/store/modules/AuthModule'

import Swal from "sweetalert2/dist/sweetalert2.min.js";

import store from '@/store'


export interface IAnswer {
  id: number;
  candidate: User;
  text: string,
  question: IQuestion,
  is_correct: boolean 
}

export interface IQuestion {
  id: number;
  author: User | null;
  title: string;
  text: string;
  is_tech: boolean;
  filled: boolean;
}

export interface IWorksheet {
  id: number;
  title: string;
  description: string | null;
  position: {
    id: number;
    title: string;
    priority: 'High' | 'Medium' | 'Low' | 'Default';
  };
  questions: Array<IQuestion>;
  answers: Array<IAnswer>;
  created_date: string;
}

export interface WorksheetInfo {
  current_worksheet: IWorksheet;
  worksheets: Array<IWorksheet>;
  current_question: IQuestion | null;
  questions: Array<IQuestion>;
  current_answer: IAnswer | null;
}


@Module
export default class PositionModule extends VuexModule implements WorksheetInfo {
  current_worksheet = {} as IWorksheet;
  worksheets = [] as Array<IWorksheet>;
  current_question: IQuestion | null = null;
  questions = [] as Array<IQuestion>;
  current_answer: IAnswer | null = null;

  get currentWorksheet(): IWorksheet {
    return this.current_worksheet;
  }

  get currentQuestion(): IQuestion | null {
    return this.current_question;
  }

  get currentAnswer(): IAnswer | null {
    return this.current_answer;
  }

  get getWorksheets(): Array<IWorksheet> {
    return this.worksheets;
  }

  get getQuestions(): Array<IQuestion> {
    return this.questions;
  }

  @Mutation
  SET_WORKSHEETS(worksheets: Array<IWorksheet>) {
    this.worksheets = worksheets;
  }

  @Mutation
  SET_WORKSHEET(worksheet: IWorksheet) {
    this.current_worksheet = worksheet;
  }

  @Mutation
  SET_QUESTION(question: IQuestion | null) {
    this.current_question = question;
    this.current_answer = null;
  }

  @Mutation
  SET_ANSWER(answer: IAnswer | null) {
    this.current_answer = answer;
  }

  @Mutation
  SET_QUESTIONS(questions: Array<IQuestion>) {
    this.questions = questions;
  }

  @Action
  async WORKSHEET_LISTING() {
    const data = await ApiService.query('worksheets.json');

    if (store.getters.status) {
      this.context.commit('SET_WORKSHEETS', data);
    }
  }

  @Action
  async QUESTION_CANDIDATE_LISTING(id: number) {
    const data = await ApiService.query('questions_candidate.json', {
      params: {
        worksheet: id
      }
    });

    if (store.getters.status) {
      this.context.commit('SET_QUESTIONS', data);
    }
  }

  @Action
  async GET_WORKSHEET(slug: string) {
    const data = await ApiService.get('worksheets', slug);

    if (store.getters.status) {
      this.context.commit('SET_WORKSHEET', data);
    }
  }

  @Action
  async CREATE_WORKSHEET(values: AxiosRequestConfig) {
    const data = await ApiService.post('worksheets/', values);

    if (store.getters.status) {
      await this.context.dispatch('GET_POSITION', data.position.id);

      Swal.fire({
        text: "Анкета успешно создана!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async OPEN_WORKSHEET(data: {id: number, positionID: number}) {
    await ApiService.update(`worksheets/${data.id}/open/`, {});

    if (store.getters.status) {
      await this.context.dispatch('GET_POSITION', data.positionID);

      Swal.fire({
        text: "Анкета успешно открыта для кандидата!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async UPDATE_WORKSHEET(values: { data: AxiosRequestConfig, id: number }) {
    const data = await ApiService.update(`worksheets/${values.id}/`, values.data);

    if (store.getters.status) {
      this.context.commit('SET_WORKSHEET', data);

      Swal.fire({
        text: "Данные успешно обновлены!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async DELETE_WORKSHEET(id: number) {
    await ApiService.delete(`worksheets/${id}/`);

    if (store.getters.status) {
      Swal.fire({
        text: "Анкета успешно удалена!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async CREATE_QUESTION(values: AxiosRequestConfig) {
    await ApiService.post('questions/', values);

    if (store.getters.status) {
      await this.context.dispatch('GET_WORKSHEET', values["worksheet"]);

      Swal.fire({
        text: "Вопрос успешно создан!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async UPDATE_QUESTION(values: { data: AxiosRequestConfig, id: number }) {
    const data = await ApiService.update(`questions/${values.id}/`, values.data);

    if (store.getters.status) {
      await this.context.commit('SET_QUESTION', data);
      await this.context.dispatch('GET_WORKSHEET', data.worksheet);

      Swal.fire({
        text: "Данные успешно обновлены!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async ANSWER(data: {values: AxiosRequestConfig, id: number}) {
    await ApiService.post('questions_candidate/answer/', {
      params: {
        worksheet: data.id
      },
      data: data.values
    })

    if (store.getters.status) {
      Swal.fire({
        text: "Ответ отправлен!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async DELETE_QUESTION(data: { id: number, worksheetID: number }) {
    await ApiService.delete(`questions/${ data.id }/`);

    if (store.getters.status) {
      await this.context.dispatch('GET_WORKSHEET', data.worksheetID);
      Swal.fire({
        text: "Вопрос успешно удален!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async DELETE_ANSWER(data: { id: number, worksheetID: number }) {
    await ApiService.delete(`answers/${ data.id }/`);

    if (store.getters.status) {
      await this.context.dispatch('GET_WORKSHEET', data.worksheetID);

      Swal.fire({
        text: "Ответ успешно удален!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }
}
