import { Module, Action, VuexModule } from "vuex-module-decorators";
import ApiService from "@/core/services/ApiService";


@Module
export default class ConfigureFilesModule extends VuexModule {
  @Action
  async GET_FILE(
    payload: {
      url: string,
      name: string
    }) {
    const response = await fetch(payload.url);
    const blob = await response.blob();
    const fileArrays = [
      new File (
        [blob],
        `${payload.name}.png`,
        {
          type: "image/png",
          lastModified: new Date().getTime()
        }
      )
    ];

    return fileArrays;
  }
}