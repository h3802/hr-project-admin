import ApiService from "@/core/services/ApiService";
import JwtService from "@/core/services/JwtService";
import { AxiosRequestConfig } from "axios";
import { Module, Action, Mutation, VuexModule } from "vuex-module-decorators";

import Swal from "sweetalert2/dist/sweetalert2.min.js";

import router from '../../router'
import store from '../../store'


export interface User {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  role: 'Supervisor' | 'HR' | 'Technical manager' | 'Technical expert';
}

export interface UserAuthInfo {
  user: User;
  isAuthenticated: boolean;
}

export interface IMember {
  current_member: User;
  members: Array<User>;
}

@Module
export default class AuthModule extends VuexModule implements UserAuthInfo, IMember {
  user = {} as User;
  isAuthenticated = !!JwtService.getToken();
  current_member = {} as User;
  members: Array<User> = [];

  get currentUser(): User {
    return this.user;
  }

  get isUserAuthenticated(): boolean {
    return this.isAuthenticated;
  }

  get getMembers(): Array<User> {
    return this.members;
  }

  get getMember(): User {
    return this.current_member;
  }

  @Mutation
  SET_AUTH(
    data: {
      auth_token: string,
    }
  ) {
    this.isAuthenticated = true;
    JwtService.saveToken(data.auth_token);
  }

  @Mutation
  SET_USER(user: User) {
    this.user = user;
    ApiService.setParams();
  }

  @Mutation
  PURGE_AUTH() {
    this.isAuthenticated = false;
    this.user = {} as User;
    JwtService.destroyToken();
    ApiService.removeHeader();
    ApiService.removeParams();
  }

  @Mutation
  SET_MEMBERS(members: Array<User>) {
    this.members = members;
  }

  @Mutation
  SET_MEMBER(member: User) {
    this.current_member = member;
  }

  @Action
  async LOGIN(credentials: AxiosRequestConfig) {
    const data = await ApiService.post("auth/token/login/", credentials);

    if (store.getters.status) {
      this.context.commit('SET_AUTH', data);
    }
  }

  @Action
  LOGOUT() {
    this.context.commit('PURGE_AUTH');
  }

  @Action({rawError: true})
  async REGISTER(credentials: AxiosRequestConfig) {
    await ApiService.post("auth/users/", credentials)

    if (store.getters.status) {
      Swal.fire({
        text: "Проверьте почту!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async CANDIDATE_REGISTER(credentials: AxiosRequestConfig) {
    await ApiService.post("candidates/", credentials)

    if (store.getters.status) {
      await this.context.dispatch('LOGIN', credentials)
    }
  }

  @Action
  async ACTIVATION(credentials: AxiosRequestConfig) {
    await ApiService.post("auth/users/activation/", credentials)

    if (store.getters.status) {
      router.push({ name: "sign-in" })

      Swal.fire({
        text: "Регистрация прошла успешно!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async SET_PASSWORD(credentials: AxiosRequestConfig) {
    await ApiService.post("auth/users/set_password/", credentials)

    if (store.getters.status) {
      Swal.fire({
        text: "Пароль успешно изменен!",
        icon: "success",
        confirmButtonText: "OK",
        buttonsStyling: false,
        customClass: {
          confirmButton: "btn btn-light-primary",
        },
      })
    }
  }

  // @Action
  // FORGOT_PASSWORD(payload: AxiosRequestConfig) {
  // }

  @Action
  async MEMBER_LISTING() {
    const data = await ApiService.query('members/');

    if (store.getters.status) {
      this.context.commit('SET_MEMBERS', data);
    }
  }

  @Action
  async GET_MEMBER(slug: string) {
    const data = await ApiService.get('members', slug);

    if (store.getters.status) {
      this.context.commit('SET_MEMBER', data);
    }
  }

  @Action
  async CREATE_MEMBER(credentials: AxiosRequestConfig) {
    await ApiService.post('member/', credentials);

    if (store.getters.status) {
      await this.context.dispatch('MEMBER_LISTING');

      Swal.fire({
        text: "Новый участник успешно добавлен! Ожидайте, когда он подтвердит приглашение...",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async VERIFY_AUTH() {
    if (JwtService.getToken()) {
      ApiService.setHeader();
      const data = await ApiService.query("auth/users/me/");
      
      if (store.getters.status) {
        this.context.commit('SET_USER', data);
      }
    } else {
      this.context.commit('PURGE_AUTH');
    }
  }

  @Action
  async UPDATE_USER(values: AxiosRequestConfig) {
    values['avatar'] = values['avatar'][0];

    const payload = new FormData();

    for (const key in values) {
      if (Object.prototype.hasOwnProperty.call(values, key)) {
        const value = values[key];
        
        payload.append(key, value);
      }
    }

    const data = await ApiService.update(
      "auth/users/me/",
      payload,
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      } as AxiosRequestConfig
    );

    if (store.getters.status) {
      this.context.commit('SET_USER', data);
      
      Swal.fire({
        text: "Данные успешно обновлены!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }
}
