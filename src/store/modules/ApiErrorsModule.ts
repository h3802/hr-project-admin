import { Module, Action, Mutation, VuexModule } from "vuex-module-decorators";


@Module
export default class ApiErrorsModule extends VuexModule {
  success = false
  errors: Array<string> = [];

  get status(): boolean {
    return this.success
  }

  get getErrors(): Array<string> {
    return this.errors;
  }

  @Mutation
  SET_ERRORS(errors: Array<string>) {
    this.success = false
    this.errors = errors;
  }

  @Mutation
  SET_SUCCESS() {
    this.success = true;
    this.errors = []
  }

  @Action
  SUCCESS() {
    this.context.commit('SET_SUCCESS')
  }

  @Action
  FAIL(errors: Array<string> = []) {
    this.context.commit('SET_ERRORS', errors)
  }
}
