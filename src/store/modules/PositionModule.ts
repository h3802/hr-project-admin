import ApiService from "@/core/services/ApiService";
import { AxiosRequestConfig } from "axios";
import { Module, Action, Mutation, VuexModule } from "vuex-module-decorators";

import Swal from "sweetalert2/dist/sweetalert2.min.js";

import store from '@/store'


export interface IPosition {
  id: number;
  title: string;
  priority: 'High' | 'Medium' | 'Low' | 'Default';
  status: 'Active' | 'Opened' | 'Closed';
  created_date: string;
}

export interface PositionsInfo {
  current_position: IPosition;
  positions: Array<IPosition>
}


@Module
export default class PositionModule extends VuexModule implements PositionsInfo {
  current_position = {} as IPosition;
  positions = [] as Array<IPosition>;

  get currentPosition(): IPosition {
    return this.current_position;
  }

  get getPositions(): Array<IPosition> {
    return this.positions;
  }

  @Mutation
  SET_POSITIONS(positions: Array<IPosition>) {
    this.positions = positions;
  }

  @Mutation
  SET_POSITION(position: IPosition) {
    this.current_position = position;
  }

  @Action
  async POSITION_LISTING() {
    const data = await ApiService.query('positions.json');

    if (store.getters.status) {
      this.context.commit('SET_POSITIONS', data);
    }
  }

  @Action
  async GET_POSITION(slug: string) {
    const data = await ApiService.get('positions', slug);

    if (store.getters.status) {
      this.context.commit('SET_POSITION', data);
    }
  }

  @Action
  async CREATE_POSITION(values: AxiosRequestConfig) {
    await ApiService.post('positions/', values);

    if (store.getters.status) {
      await this.context.dispatch('POSITION_LISTING');

      Swal.fire({
        text: "Вакансия успешно создана!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async UPDATE_POSITION(values: { data: AxiosRequestConfig, id: number }) {
    const data = await ApiService.update(`positions/${values.id}/`, values.data);

    if (store.getters.status) {
      this.context.commit('SET_POSITION', data);

      Swal.fire({
        text: "Данные успешно обновлены!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async OPEN_POSITION(id: number) {
    await ApiService.update(`positions/${id}/open/`, {});

    if (store.getters.status) {
      await this.context.dispatch('POSITION_LISTING');

      Swal.fire({
        text: "Вакансия успешно открыта для HR!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async CLOSE_POSITION(id: number) {
    await ApiService.update(`positions/${id}/close/`, {});

    if (store.getters.status) {
      await this.context.dispatch('POSITION_LISTING');

      Swal.fire({
        text: "Вакансия успешно архивирована!",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  @Action
  async DELETE_POSITION(id: number) {
    await ApiService.delete(`positions/${id}/`);
  }
}
