import { createApp } from "vue";
import App from "./App.vue";

// import axios from "axios";
// import VueAxios from "vue-axios";
import router from "./router";
import store from "./store";
// import ElementPlus from "element-plus";
import i18n from "@/core/plugins/i18n";

import ApiService from "@/core/services/ApiService";
import { initApexCharts } from "@/core/plugins/apexcharts";
import { initInlineSvg } from "@/core/plugins/inline-svg";
import { initVeeValidate } from "@/core/plugins/vee-validate";

import "@/core/plugins/prismjs";

const app = createApp(App);

app.use(store);
app.use(router);
// app.use(VueAxios, axios);
// app.use(ElementPlus);
app.use(i18n);

ApiService.init();

initApexCharts(app);
initInlineSvg(app);
initVeeValidate();

app.mount("#app");
