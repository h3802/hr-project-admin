import { createI18n } from "vue-i18n/index";

const messages = {
};

const i18n = createI18n({
  legacy: false,
  locale: "ru",
  globalInjection: true,
  messages,
});

export default i18n;
