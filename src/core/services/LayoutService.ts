import { Actions } from "@/store/enums/StoreEnums";
import store from "@/store/";

class LayoutService {
  /**
   * @description initialize default layout
   */
  public static init(): void {
    LayoutService.initLayout();
    LayoutService.initHeader();
    LayoutService.initToolbar();
  }

  /**
   * @description init layout
   */
  public static initLayout(): void {
    store.dispatch(Actions.ADD_BODY_ATTRIBUTE, {
      qulifiedName: "id",
      value: "kt_body",
    });

    store.dispatch(Actions.ADD_BODY_CLASSNAME, "page-loading-enabled");
    store.dispatch(Actions.ADD_BODY_CLASSNAME, "page-loading");
  }

  /**
   * @description init header
   */
  public static initHeader(): void {
    store.dispatch(Actions.ADD_BODY_CLASSNAME, "header-fixed");

    store.dispatch(
      Actions.ADD_BODY_CLASSNAME,
      "header-tablet-and-mobile-fixed"
    );
  }

  /**
   * @description init toolbar
   */
  public static initToolbar(): void {
    store.dispatch(Actions.ADD_BODY_CLASSNAME, "toolbar-enabled");

    store.dispatch(Actions.ADD_BODY_CLASSNAME, "toolbar-fixed");
  }

  /**
   * @description init aside
   */
  public static initAside(): void {
    store.dispatch(Actions.ADD_BODY_CLASSNAME, "aside-enabled");

    store.dispatch(Actions.ADD_BODY_CLASSNAME, "aside-fixed");
  }
}

export default LayoutService;
