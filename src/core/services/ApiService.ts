import JwtService from "@/core/services/JwtService";
import axios, { AxiosInstance, AxiosResponse, AxiosRequestConfig } from "axios";

import router from '../../router'
import store from '../../store'

import Swal from "sweetalert2/dist/sweetalert2.min.js";


export default class ApiService {
  private static axiosInstance: AxiosInstance;

  public static init() {
    const config: AxiosRequestConfig = {
      baseURL: process.env.VUE_APP_API_URL,
      headers: {
        'Accept': 'application/json',
      },
      params: {}
    };

    this.axiosInstance = axios.create(config);
  }

  public static setHeader(): void {
    this.axiosInstance.defaults.headers.common[
      "Authorization"
    ] = `Token ${JwtService.getToken()}`;
  }

  public static removeHeader(): void {
    delete this.axiosInstance.defaults.headers.common["Authorization"]
  }

  public static setParams(): void {
    if (!store.getters.currentUser.company) {
      return;
    }

    this.axiosInstance.defaults.params[
      "USER_ACCOUNT"
    ] = store.getters.currentUser.company.id
  }

  public static removeParams(): void {
    delete this.axiosInstance.defaults.params["USER_ACCOUNT"]
  }


  private static async handleResponse(
    response: Promise<AxiosResponse> 
  ) {
    return await response
    .then(({ data }) => {
      store.dispatch('SUCCESS');
      return data
    }).catch(({ response }) => {
      store.dispatch('FAIL')

      if (response.status >= 500) {
        router.push({ name: '500' });
      } else if (response.status === 404) {
        router.push({ name: '404' });
      } else if (response.status === 401) {
        store.dispatch('LOGOUT');
      } else {
        const text: string = response.data.message || response.data.non_field_errors || 'Что-то пошло не так...';
  
        Swal.fire({
          text: text,
          icon: "error",
          buttonsStyling: false,
          confirmButtonText: "Попробуйте еще раз!",
          customClass: {
            confirmButton: "btn fw-bold btn-light-danger",
          },
        });
  
        store.dispatch('FAIL', response.data)
      }
    });
  }

  public static query(
    url: string,
    params: AxiosRequestConfig = {}
  ) {
    const response = this.axiosInstance.get(url, params);
    return this.handleResponse(response);
  }

  public static post(
    url: string,
    data: AxiosRequestConfig
  ) {
    const response = this.axiosInstance.post(url, data)
    return this.handleResponse(response)
  }

  public static get(
    url: string,
    slug: string
  ) {
    const response = this.axiosInstance.get(`${url}/${slug}`);
    return this.handleResponse(response);
  }

  public static update(
    url: string,
    data: unknown,
    config: AxiosRequestConfig = {}
  ) {
    const response = this.axiosInstance.patch(url, data, config);
    return this.handleResponse(response);
  }

  public static delete(
    url: string,
    data: AxiosRequestConfig = {}
  ) {
    const response = this.axiosInstance.delete(url, data);
    return this.handleResponse(response);
  }
}
